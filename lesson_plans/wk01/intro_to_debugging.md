## Intro to Debugging

#### What is Debugging <a name="what-is-debugging"></a>

[Debugging](https://www.techopedia.com/definition/16373/debugging) is the process of locating and removing computer program bugs, errors, or abnormalities.

#### What is a Debugger <a name="what-is-a-debugger"></a>

In order to achieve the process of debugging, software developers have access to debugging tools. One of these kinds of tools is known as a debugger. A debugger allows you to stop or halt the program according to specific conditions and to control program flow manually. 

#### Debugging Actions

#### GDB Cheat Sheet

#### Debugging Strategies

[Sauce: Debugging - The 9 Indispensible Rules for Finding Even the Most Elusive Software and Hardware Problems](https://www.amazon.com/Debugging-Indispensable-Software-Hardware-Problems/dp/0814474578?keywords=debugging+9+indispensable+rules&qid=1539178732&s=Books&sr=1-1-fkmrnull&ref=sr_1_fkmrnull_1)

1. *Understand the System:* “You need a working knowledge of what the system is supposed to do, how it’s designed, and, in some cases, why it was designed that way. If you don’t understand some part of the system, that always seems to be where the problem is.” __Read the F.... Manual__
1. *Make It Fail:* You want to make it fail so that you can look at it, so you can focus on the cause, and so you can tell if you’ve fixed it. *Simulate* the conditions that **stimulate** the failure, but don’t *simulate* the failure itself.
1. *Quit Thinking and Look:* “It is a capital mistake to theorize before one has data. Insensibly one begins to twist facts to suit theories, instead of theories to suit facts.” -- Sherlock Holmes. If you guess at how something is failing, you often fix something that isn’t the bug
1. *[Divide and Conquer](https://en.wikipedia.org/wiki/Divide_and_conquer_algorithm)*: Split up your search space into a good half and bad half and find where the issue is efficiently! 
1. *Change One Thing at a Time:* If you change ten, how can you be sure which of your changes actually fixed the problem?
1. *Keep an Audit Trail:* Write down what you did, in what order, and what happened
1. *Check the Plug:* Have you turned it off and on again? Is it plugged in? Don’t discount the “obvious things” and assume the general requirements. Question your assumptions. Rarely, it might be a problem with your tool. Your tool was built by engineers like you, why would it be any more trustworthy than what you’re building?
1. *Get a Fresh View:* Find your rubber ducky. Try a human rubber ducky.
1. *If You Didn’t Fix It, It Ain’t Fixed:* Confirm the fix is done. Don’t just swap out what you think is wrong and never take the time to check your work!
